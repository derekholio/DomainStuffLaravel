var gulp = require('gulp');
var cleanCSS = require('gulp-clean-css');
var minify = require('gulp-minify');
var concat = require('gulp-concat');

gulp.task('minify-css', function() {
  return gulp.src('resources/assets/css/*.css')
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(gulp.dest('public/css'));
});

// minify and combine all JS
gulp.task('scripts', function() {
  return gulp.src('resources/assets/js/*.js')
    .pipe(concat('combo.js'))
    .pipe(minify())
    .pipe(gulp.dest('public/js/'));
});

gulp.task('default', ['minify-css', 'scripts']);