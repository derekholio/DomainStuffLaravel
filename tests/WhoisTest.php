<?php

class WhoisTest extends TestCase
{
    public function testWhois()
    {
        echo "\n";
        for($i = 0; $i < 100; $i++){
            $domain = str_random() . "." . str_random(3);
            echo "Testing $domain\n";
            $response = $this->call('GET', "/api/lookup/whois/$domain");
            $this->assertEquals(200, $response->status());
            echo $response->content() . "\n";
        }
    }
}
