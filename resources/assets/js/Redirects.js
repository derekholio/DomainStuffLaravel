function getRedirects(domain){
    $.ajax({
        url: "/api/lookup/redirect/"+domain,
        dataType: 'json',
        success: function(data){
            if(data.error){
                content = "<div class='containerEntry'>"
                content += "<div class='containerContent bad'>" + data.error + "</div>";
                content += "</div>";
            }else{
                var content = "<div class='panel-group' id='accordion'>";
                content += "<div class='panel panel-primary'>";
                content += "<div class='panel-heading'>"
                content += "<h4 class='panel-title'>"
                content += "<a data-toggle='collapse' data-parent='#accordion'>"
                content += "Starting at http://" + domain;
                content += "</a>"
                content += "</h4>";
                content += "</div>";
                content += "</div>";


                $.each(data, function(firstIndex, value){
                    var movedto = value.movedto;

                    if(firstIndex == data.length){

                        if(value.status == 200){
                            content += "<div class='panel panel-success'>";
                        }else{
                            content += "<div class='panel panel-danger'>";
                        }
                        
                        content += "<div class='panel-heading'>";
                        content += "<h4 class='panel-title'>";
                        content += "<a data-toggle='collapse' data-parent='#accordion' href='#collapse"+firstIndex+"'>";
                        if(value.status == 200){
                            content += "Ended at: " + value.movedto + " (" + value.status + " " + value.phrase + ")";
                        }else{
                            content += "Ended at: " + value.movedto + " (" + value.status + " " + value.phrase + ") [TOO MANY REDIRECTS]";
                        }
                        
                        content += "</a>";
                        content += "</div>";
                    }else{

                        if(value.status != 200){
                            content += "<div class='panel panel-info'>";
                        }else{
                            content += "<div class='panel panel-success'>";
                        }
                        
                        content += "<div class='panel-heading'>";
                        content += "<h4 class='panel-title'>";
                        content += "<a data-toggle='collapse' data-parent='#accordion' href='#collapse"+firstIndex+"'>";
                        content += value.movedto + " (" + value.status + " " + value.phrase + ")";
                        content += "</a>";
                        content += "</div>";
                    }
                    

                    content += "<div id='collapse"+firstIndex+"' class='panel-collapse collapse'>";
                    content += "<div class='panel-body'>";
                    $.each(value.headers, function(secondIndex, value){
                        if(value.length > 1){
                            $.each(value, function(thirdIndex, value){
                                content += "<div>" + secondIndex + ": " + value + "</div>";
                            });
                        }else{
                            content += "<div>" + secondIndex + ": " + value + "</div>";
                        }
                        
                    });
                    content += "</div>";
                    content += "</div>";
                    content += "</div>";
                });

                content += "</div>";
            }

            
            $("#RedirectChecker").html(content);
        }
    })
}