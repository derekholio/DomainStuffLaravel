function getGeoIP(domain){
    $.ajax({
        url: "/api/lookup/geoip/"+domain
    })
    .fail(function(data){
        $("#getLoc").replaceWith("Failed to get geo-location for " + domain);
        console.log(data);
    })
    .done(function(data){
        if(data.error){
            $("#geoLoc").html(data.error);
        }else{
            var city = "";
            var region = "";
            var country = "";
            var org = "";
            if(data.city){
                city = data.city + ', ';
            }
            if(data.region){
                region = data.region + ', ';
            }
            if(data.country){
                country = data.country;
            }
            if(data.org){
                org = ' ('+data.org+')';
            }
            
            var str = city+region+country+org;
            
            $("#geoLoc").replaceWith(str);
        }
    })
}