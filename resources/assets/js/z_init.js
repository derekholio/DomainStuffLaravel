$(document).ready(function(){
    var domain = window.location.pathname;
    domain = domain.replace('/', '');
    
    if(domain){//Only make calls if a domain is specified
        getDNS(domain);
        getGeoIP(domain);
        fillDomain(domain);
        getWHOIS(domain);
        getGlobalDNS(domain);
        getPortScan(domain);
        getApps(domain);
        getSSL(domain);
        getRedirects(domain);
        getScreenshot(domain);
    }
});