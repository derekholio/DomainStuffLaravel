function getScreenshot(domain){
    $.ajax({
        url: "/api/lookup/screenshot/"+domain,
        dataType: 'json',
        success: function(data){
            var screenshot = data.screenshot;
            content = "<img src=//" + window.location.hostname + "/" + screenshot + ">";

            $("#SnapshotContent").html(content);
        }
    });
}