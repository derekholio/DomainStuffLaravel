function getDNS(domain){
    var mail_ips = []
    $.ajax({
        url: "/api/lookup/dns/"+domain
    })
    .fail(function(data){
        var HTML = "<div class='containerEntry'><div class='containerContent bad'>Failed to lookup DNS (server error)</div></div>";
        $("#DNS").html(HTML);
        $("#RDNS").html(HTML);
    })
    .done(function(data){
        console.log(data);
        var DNS_HTML = "<div class='containerEntry'>";
        var RDNS_HTML = "<div class='containerEntry'>";
        if(data.error){
            DNS_HTML += ("<div class='containerContent bad'>" + data.error + "</div>");
        }else{
            var a_records = data.A;
            if(a_records){
                a_records.forEach(function(element, index, array){
                    DNS_HTML += ("<div class='containerContent good'><b>A: </b>" + element.A + "</div>");
                    RDNS_HTML += ("<div class='containerContent'><b>[A] " + element.A + "</b>: " + element.PTR + "</div>");
                });
            }else{
                DNS_HTML += ("<div class='containerContent bad'><b>A: </b>No A records were found</div>");
                RDNS_HTML += ("<div class='containerContent'>No A records were found</div>");
            }

            var aaaa_records = data.AAAA;
            if(aaaa_records){
                aaaa_records.forEach(function(element, index, array){
                    DNS_HTML += ("<div class='containerContent good'><b>AAAA: </b>" + element.AAAA + "</div>");
                });
            }else{
                DNS_HTML += ("<div class='containerContent warning'><b>AAAA: </b>No AAAA records were found</div>");
            }

            var caa_records = data.CAA;
            console.log(caa_records);
            if(caa_records){
                caa_records.forEach(function(element, index, array){
                    DNS_HTML += ("<div class='containerContent good'><b>CAA: </b>" + element.CAA + "</div>");
                });
            }else{
                DNS_HTML += ("<div class='containerContent warning'><b>CAA: </b>No CAA records were found</div>");
            }

            var mx_records = data.MX;
            if(mx_records){
                mx_records.forEach(function(element, index, array){
                    DNS_HTML += ("<div class='containerContent good'><b>MX: </b>" + element.Priority + ":" + element.Record + " (A: " + element.A + ")</div>");
                    RDNS_HTML += ("<div class='containerContent'><b>[MX] " + element.A + ":</b> " + element.PTR + "</div>");
                    mail_ips.push(element.A);
                });
            }else{
                DNS_HTML += ("<div class='containerContent warning'><b>MX: </b>No MX records were found</div>");
            }
            
            var ns_records = data.NS;
            if(ns_records){
                ns_records.forEach(function(element, index, array){
                    DNS_HTML += ("<div class='containerContent good'><b>NS: </b>" + element.NS + " (" + element.A + ")</div>");
                });
            }else{
                DNS_HTML += ("<div class='containerContent bad'><b>NS: </b>No NS records were found</div>");
            }

            var soa_record = data.SOA;
            if(soa_record){
                DNS_HTML += ("<div class='containerContent good'><b><div class='contentTitle'>SOA</div></b>");
                DNS_HTML += ("Authority: " + soa_record[0].Record + "<br />");
                DNS_HTML += ("Responsible: " + soa_record[0].Responsible + "<br />");
                DNS_HTML += ("Serial: " + soa_record[0].Serial + "<br />");
                DNS_HTML += ("Refresh: " + soa_record[0].Refresh + "<br />");
                DNS_HTML += ("Expiry: " + soa_record[0].Expiry + "<br />");
                DNS_HTML += ("Time to live: " + soa_record[0]["Minimum TTL"] + "<br />");
                DNS_HTML += "</div>";
            }else{
                DNS_HTML += ("<div class='containerContent bad'><b>SOA: </b>No SOA records were found</div>");
            }

            var txt_records = data.TXT;
            if(txt_records){
                txt_records.sort();
                txt_records.forEach(function(element, index, array){
                    if(element.startsWith("v=spf")){
                        DNS_HTML += ("<div class='containerContent good'><b>SPF: </b>" + element + "</div>");
                    }else{
                        DNS_HTML += ("<div class='containerContent good'><b>TXT: </b>" + element + "</div>");
                    }
                });
            }else{
                DNS_HTML += ("<div class='containerContent warning'><b>TXT: </b>No TXT/SPF records were found</div>");
            }
        }

        DNS_HTML += "</div>";
        RDNS_HTML += "</div>";
        $("#DNS").html(DNS_HTML);
        $("#RDNS").html(RDNS_HTML);

        var count = mail_ips.length;
        if(count == 0){
            $("#Blacklists").html("<div class='containerEntry'><div class='containerContent warning'>No MX records were found</div></div>")
        }else{
            var BL_HTML = "<div class='containerEntry'>";
            var deferreds = [];
            mail_ips.forEach(function(element, index, array){//Start each AJAX call and push it to the deferreds, so $.when.apply can process it
                deferreds.push(
                    $.ajax({
                        url: '/api/lookup/blacklist/' + element
                    })
                    .done(function(data){
                        var temp = "<div class='contentContent'><b><div class='contentTitle'>"+element+"</div></b>";//use temp HTML holder, otherwise AJAX may appear HTML in the wrong order as threads finish
                        for(var key in data){
                            var entry = data[key];
                            if(entry.length == 0){
                                temp += "<div class='containerContent good'><b>"+key+"</b>: Not listed</div>";
                            }else{
                                temp += "<div class='containerContent bad'><b>"+key+"</b>: " + entry.join(", ") + "</div>";
                            }
                        }

                        BL_HTML += temp;
                    })
            )});

            $.when.apply($, deferreds).done(function(){
                BL_HTML += "</div>";
                $("#Blacklists").html(BL_HTML);  
            });
        }
    })
}