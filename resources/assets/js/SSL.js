function getSSL(domain){
    $.ajax({
        url: "/api/lookup/ssl/"+domain,
        dataType: 'json',
        success: function(data){
            var content = "<div class='containerEntry'>";

            if(data.error){
                content += "<div class='containerContent bad'>";
                content += data.error;
                content += "</div>";
            }else{
                if(data.valid){
                    content += "<div class='containerContent good'><b>Valid: </b>Certificate is valid</div>";
                }else{
                    content += "<div class='containerContent bad'><b>Valid: </b>Certificate is not valid</div>";
                }
                
                content += "<div class='containerContent'><b>Issuer:</b> " + data.issuer + "</div>";
                content += "<div class='containerContent'><b><div class='contentTitle'>Hostnames</div></b>" + data.SAN.join("<br />") + "</div>";
                content += "<div class='containerContent'><b>IP scanned:</b> " + data.ip + "</div>";
                content += "<div class='containerContent'><b>Expiration:</b> " + data.expiration + "</div>";
                content += "<div class='containerContent'><b>Signature:</b> " + data.signatureTypeSN + " / " + data.signatureTypeLN + "</div>";

                if(data.warnings.length > 0){
                    content += "<div class='containerContent warning'><b><div class='contentTitle'>Warnings</div></b>" + data.warnings.join("<br />") + "</div>";
                }
            }

            content += "</div>";
            $("#SSLChecker").html(content);
        }
    });
}