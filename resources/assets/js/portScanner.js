function getPortScan(domain){
    $.ajax({
        url: "/api/lookup/portscan/"+domain,
        dataType: 'json',
        success: function(data){
            var content = "<div class='containerEntry'>";
            $.each(data, function(index, value){
                var port = value.port;
                var service = value.service;
                var open = value.open;

                var good_or_bad = (open == true ? 'good' : 'bad');
                var open_or_closed = (open == true ? 'Open' : 'Closed');
                content += "<div class='containerContent " + good_or_bad + "'>";
                content += '<b>' + port + ' (' + service + ') : ' + open_or_closed;
                content += '</div>';
            });

            content += "</div>";
            $("#PortScan").html(content);
        }
    })
}