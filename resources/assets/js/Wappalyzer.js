function getApps(domain){
    $.ajax({
        url: "/api/lookup/wappalyzer/"+domain,
        dataType: 'json',
        success: function(data){
            console.log(data);
            var content = "<div class='containerEntry'>";
            if(!data.applications){
                content += "<div class='containerContent'>No apps detected</div>";
            }else{
                var apps = data.applications;
                $.each(apps, function(index, value){
                    content += "<div class='containerContent'>";
                    content += "<p class='contentTitle'>";
                    content += value.categories[0];
                    content += "</p>";

                    content += "<p class='CMS'>";
                    content += "<img src='/img/icons/" + value.icon + "' class='CMSImg'>";
                    content += value.name;

                    if(value.version !== ""){
                        content += ' (' + value.version + ')';
                    }

                    content += "</p>";

                    content += "</div>";
                });

                content += "</div>";
            }
            $("#CMSCheck").html(content);
        }
    })
}