function getWHOIS(domain){
    $.ajax({
        url: "/api/lookup/whois/"+domain
    })
    .fail(function(data){
        console.log(data);
    })
    .done(function(data){
        $("#WhoisContent").html(data);
    })
}