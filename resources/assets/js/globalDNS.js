function getGlobalDNS(domain){
    $.ajax({
        url: "/api/lookup/globallookup/"+domain,
        dataType: 'json',
        success: function(data){
            var content = "<div class='containerEntry list'>";
            $.each(data, function(index, value){
                var provider = value.provider;
                if(value.results.length > 0){
                    var results = value.results.join("<br />");
                    
                }else{
                    var results = "No records";
                }

                content += "<div class='containerContent'><p class='contentTitle'>"+provider+"</p>"+results+"</div>";
            });
            content += "</div>";
            $("#WhatsMyDNS").html(content);
        }
    });
}