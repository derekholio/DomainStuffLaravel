@extends("layouts.master")

@section('title', $domain)

@section('content')
<div id="tabs">
    <!-- TABS LIST -->
    <ul>
        <li id="DNS-TAB"><a href="#DNS">DNS Records</a></li>
        <li id="RDNS-TAB"><a href="#RDNS">Reverse DNS</a></li>
        <li id="BLACKLIST-TAB"><a href="#Blacklists">Blacklists</a></li>
        <li id="DNSPROP-TAB"><a href="#DNSProp">DNS Propagation</a></li>
        <li id="SNAPSHOT-TAB"><a href="#Snapshot">Snapshot</a></li>
        <li id="CMS-TAB"><a href="#CMS">CMS</a></li>
        <li id="REDIRECT-TAB"><a href="#Redirect">Redirect Checker</a></li>
        <li id="SSL-TAB"><a href="#SSL">SSL Checker</a>
        <li id="WHOIS-TAB"><a href="#WHOIS">WHOIS Stack</a></li>
        <li id="PORTS-TAB"><a href="#Ports">Ports</a></li>
        <li id="FEEDBACK-TAB"><a href="#Feedback">Feedback</a></li>
    </ul>
    <!-- END TABS LIST -->
    <!-- DNS TAB -->
    <div id="DNS" class="tab">
        <div class="containerEntry">
            <div class="containerContent">
                <p class="contentTitle">Pulling DNS records...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- END DNS TAB -->
    <!-- CMS TAB -->
    <div id="CMS" class="tab">
        <div class="containerEntry list" id="CMSCheck">
            <div class="containerContent">
                <p class="contentTitle">Scanning site for apps...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- END CMS TAB -->
    <div id="Snap" class="tab">
        <div class="containerEntry" id="Snapshot">
            <div class="containerContent" id="SnapshotContent">
                <p class="contentTitle">Loading Browser Snapshot</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- REVERSE DNS TAB -->
    <div id="RDNS" class="tab">
        <div class="containerEntry">
            <div class="containerContent">
                <p class="contentTitle">Pulling reverse DNS records...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- END REVERSE DNS TAB -->
    <!-- BLACKLIST TAB -->
    <div id="Blacklists" class="tab">
        <div class="containerEntry">
            <div class="containerContent">
                <p class="contentTitle">Checking for blacklisted IPs...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- END BLACKLIST TAB -->
    <!-- REDIRECT TAB -->
    <div id="Redirect" class="tab">
        <div class="containerEntry" id="RedirectChecker">
            <div class="containerContent">
                <p class="contentTitle">Scanning URL for redirects...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- END REDIRECT TAB -->
    <!--SSL TAB-->
    <div id="SSL" class="tab">
        <div class="containerEntry" id="SSLChecker">
            <div class="containerContent">
                <p class="contentTitle">Scanning URL for SSL... (This can take a few minutes)</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your resutls are loading"/>
            </div>
        </div>
    </div>
    <!--END SSL TAB -->
    <!-- WHOIS TAB -->
    <div id="WHOIS" class="tab">
        <div class="containerEntry" id="WhoisContent">
            <div class="containerContent">
                <p class="contentTitle">Retrieving WHOIS...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your resutls are loading"/>
            </div>
        </div>
    </div>
    <!-- END WHOIS TAB -->
    <!-- DNS PROPAGATION TAB -->
    <div id="DNSProp" class="tab">
        <div class="containerEntry list" id="WhatsMyDNS">
            <div class="containerContent">
                <p class="contentTitle">Checking globally for DNS records...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- END DNS PROPAGATION TAB -->
    <!-- PORTS TAB -->
    <div id="Ports" class="tab">
        <div class="containerEntry" id="PortScan">
            <div class="containerContent" id="PortScannerContent">
                <p class="contentTitle">Scanning for ports...</p>
                <img src="{{asset('img/loading.gif')}}" alt="Your results are loading"/>
            </div>
        </div>
    </div>
    <!-- END PORTS TAB -->
    <!-- FEEDBACK TAB -->
    <div id="Feedback" class="tab">
        <div class="containerEntry">
            <div class="containerContent more">
                <div class="containerContent good" id="emailStatus" style="display: none"></div>
                <div id="mailcont">
                    <p class="contentTitle">Feedback</p>
                    <p>Have an idea you think would be useful? Let us know!</p>
                    <form id="mailForm">
                        <input type="text" id="query_domain" name="query_domain" value="%%QUERY_DOMAIN"
                               hidden>
                        <input type="email" id="emailFrom" name="emailFrom" placeholder="you@domain.com"
                               required>
                        <br/>
                        <br/>
                        <input type="text" id="emailSubject" name="emailSubject" placeholder="Your subject"
                               required>
                        <br/>
                        <br/>
                        <textarea cols="50" rows="7" id="emailMessage" name="emailMessage"
                                  placeholder="Your message" required></textarea>
                        <br/>
                        <br/>
                        <div id="recaptcha"></div>
                        <br/>
                        <input type="button" id="emailSubmit" value="Submit" disabled>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END FEEDBACK TAB -->
</div>
<!-- END TABS -->
@endsection