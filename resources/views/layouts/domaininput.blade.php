<div class="form">
    <form onsubmit="return domainFormSubmit()" method="get">
        Please specify a domain name:
        <br/>
        <br/>

        <input type="text" id="domainInput" name="domainInput" autofocus />
        <input type="submit" />
        <br/>
    </form>
    <div id='display-domain-wrapper'>
        <br />
        <p id="domainName">Showing information for: <span id="di_domainName"></span></p>
        <p>Estimated location: <span id="geoLoc"><img src="{{asset('/img/loading.gif')}}" alt="Your results are loading"/></span>
        </p>
    </div>
</div>
