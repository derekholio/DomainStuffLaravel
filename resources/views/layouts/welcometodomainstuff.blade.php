<div class="form info-wrapper">
    Welcome to DomainStuff!<br /><br />
    
    DomainStuff provides an in depth look into various details about domains. <br /><br />
    When you search for a domain in DomainStuff, you receive a number of details back about that domain.<br /><Br />
    Expand each section below to learn more!<br /><br />
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
              DNS Records</a>
            </h4>
          </div>
          <div id="collapse1" class="panel-collapse collapse">
            <div class="panel-body">
                DomainStuff will query a DNS server to retrieve the A, AAAA (IPv6), 
                MX, NS, SOA, TXT and SPF records for the entered domain.
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
              Reverse DNS Records</a>
            </h4>
          </div>
          <div id="collapse2" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will query a DNS server to retrieve the PTR records for 
                  all IP addresses from the A and MX records.
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">
              Blacklists</a>
            </h4>
          </div>
          <div id="collapse3" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will query your MX IP addresses against common blacklists
                  to alert you of potential mail issues.
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">
              DNS Propagation</a>
            </h4>
          </div>
          <div id="collapse4" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will query several different DNS servers located through out the world
                  to show you DNS propagation.
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">
              Website Snapshot</a>
            </h4>
          </div>
          <div id="collapse5" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will browse the home page of your site, and 
                  create a snapshot of it for you to view.
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">
              CMS and Technologies</a>
            </h4>
          </div>
          <div id="collapse6" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will analyze server headings and various aspects
                  of the home page to determine what CMS or technologies the site
                  is using.
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">
              Redirect Checker</a>
            </h4>
          </div>
          <div id="collapse7" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will browse to the home page of the site provided,
                  and list any redirects and status codes it experienced along
                  the way there.
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse8">
              SSL Checker</a>
            </h4>
          </div>
          <div id="collapse8" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will analyze the SSL certificate that is being used 
                  of the provided site, and return various results relating to that
                  SSL certificate. 
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse9">
              WHOIS</a>
            </h4>
          </div>
          <div id="collapse9" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will pull the WHOIS for the site provided and provide
                  it to you to see.
              </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapse10">
              Port Scan</a>
            </h4>
          </div>
          <div id="collapse10" class="panel-collapse collapse">
              <div class="panel-body">
                  DomainStuff will do a port scan for common ports and show you
                  which ports are open or closed, and what services they often
                  are related to.
              </div>
          </div>
        </div>
    </div>
</div>
