<meta charset="UTF-8">
<meta name="application-name" content="DomainStuff">
<meta name="author" content="Derek Sims, Ryan Peterson">
<meta name="keywords" content="DNS, domain, blacklist, reverse DNS, CMS, WHOIS">
<meta name="description" content="Provides a domain over view of a domain's DNS records, web application, and more">
<meta name="viewport" content="width=device-width, initial-scale=1">

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-92260588-1', 'auto');
  ga('send', 'pageview');

</script>

<title>DomainStuff</title>
<!-- Favicon -->
<link rel="icon" type="image/x-icon" href="{{asset('/favicon.ico')}}"/>

<!-- jQuery and theming -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript" src="//code.jquery.com/jquery-latest.min.js"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link href='https://fonts.googleapis.com/css?family=Kadwa' rel='stylesheet' type='text/css'>

<!-- Bootstrap -->
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<!-- Recaptcha -->
<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&amp;render=explicit" async defer></script>

<!-- Custom JS -->
<script type="text/javascript" src="{{asset('js/combo-min.js')}}"></script>

<!-- Custom CSS -->
<link rel="stylesheet" href="{{asset('css/master.css')}}">