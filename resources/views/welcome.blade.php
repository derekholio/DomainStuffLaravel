<!DOCTYPE html>
<html>
    <head>
        @include('layouts.head')
    </head>
    <body>
        <div class='container-fluid'>
            @include('layouts.domaininput')
            @include('layouts.welcometodomainstuff')
        </div>
    </body>
    <footer>
        @include('layouts.footer')
    </footer>
</html>
