<?php

namespace App\Library\GEOIP;

use Cache;

class IPLocator
{
    private $domain;
    private $domainA;
    public function __construct($domain){
        $this->domain = $domain;
    }
    
    public function lookup(){
        $cacheKey = sprintf("geoip_%s", $this->domain);
        if(Cache::has($cacheKey)){
            return Cache::get($cacheKey);
        }else{
            try{
                $response = $this->__lookup();
                Cache::put($cacheKey, $response, 1440);
                return $response;
            } catch (Exception $ex) {
                Log::error('Unhandled exception querying to ' . $this->domain);
                Log::error($ex);
                $err = array(
                    'error' => 'Failed to get geo-location for ' . $this->domain,
                );
                Cache::put($cacheKey, $err, 10);
                return $err;
            }
        }
    }
    
    private function __lookup(){
        $dnslookup = dns_get_record($this->domain, DNS_A);
        if(count($dnslookup) > 0){
            $this->domainA = $dnslookup[0]['ip'];
            $query = sprintf("http://ipinfo.io/%s/json", $this->domainA);
            $response = file_get_contents($query);
            if(str_contains($response, "valid IP")){
                return array(
                    'error' => 'Failed to get geo-location for ' . $this->domain,
                );
            }else{
                $arr = json_decode($response);
                if($arr){
                    $org = $arr->org;
                    $org = preg_replace('/^AS\d{1,}\s/', '', $org);
                    $arr->org = $org;
                    $response = $arr;
                }
                return $response;
            }
            return file_get_contents($query);
        }else{
            return array(
                'error' => 'Failed to get IP address for ' . $this->domain,
            );
        }
    }
}

?>