<?php

namespace App\Library\WHOIS;

use Cache;

class DSWHOIS
{
    private $domain;
    public function __construct($domain) {
        $this->domain = $domain;
    }
    
    public function lookup(){
        $cacheKey = sprintf("whois_%s", $this->domain);
        try{
            if(Cache::has($cacheKey)){
                return Cache::get($cacheKey);
            }else{
                $whoisQuery = new \Phois\Whois\Whois($this->domain);
                $result = $whoisQuery->info();
                Cache::put($cacheKey, $result, 1440);
                if(str_contains($result, "Domain name isn't valid")){
                    return "Invalid domain: " . $this->domain;
                }
                return $result;
            }
        } catch (Exception $ex) {
            Log::error($ex);
            return sprintf("An unexpected error was return while querying for the WHOIS for %s. This error has been logged.", $this->domain);
        }
    }
}