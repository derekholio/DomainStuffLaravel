<?php

namespace App\Library\DNS;

use Cache;//Laravel caching engine
use PurplePixie\PhpDns\DNSQuery;
use NET_DNS2\DNS2;

class DNSResolver
{
    private $domain;
    private $result_array = array();
    private $server;
    
    public function __construct($domain, $server = "8.8.8.8"){
        $this->domain = $domain;
        $this->server = $server;
    }
    
    private static function sort_mx($a, $b){
        if($a['Priority'] > $b['Priority']){
                return 1;
        }else if($a['Priority'] < $b['Priority']){
                return -1;
        }else{
                return 0;
        }
    }

    function getRecords($domain, $type){
            $lookup = new DNSQuery($this->server, 53, 3, true, false, false);
            $result = $lookup->query($domain, $type);

            if(!$result){
                    return array();
            }
            $result_count=count($result);

            $temp = array();

            if($type == "MX"){
                    for($i=0; $i<$result->count(); $i++){
                            $record = $result->current()->getData();
                            $pri = $result->current()->getExtras()['level'];
                            array_push($temp, array('Record' => $record, 'Priority' => $pri));
                            $result->next();
                    }
                    usort($temp, array($this, 'sort_mx'));
            }else if($type == "SOA"){
                    for($i=0; $i<$result->count(); $i++){
                            $soa_results = $result->current()->getExtras();
                            $record = $result->current()->getData();
                            $serial = $soa_results['serial'];
                            $refresh = $soa_results['refresh'];
                            $retry = $soa_results['retry'];
                            $expiry = $soa_results['expiry'];
                            $minttl = $soa_results['minttl'];
                            $responsible = $soa_results['responsible'];
                            array_push($temp,
                            array('Record' => $record,
                                    'Serial' => $serial,
                                    'Refresh' => $refresh,
                                    'Retry' => $retry,
                                    'Expiry' => $expiry,
                                    'Minimum TTL' => $minttl,
                                    'Responsible' => $responsible
                            ));
                            $result->next();
                    }
            }else{
                $temp = array();
                for($i=0; $i<$result->count(); $i++){
                        array_push($temp, $result->current()->getData());
                        $result->next();
                }
            }

            return $temp;
    }

    function getIPv6($domain, &$res){
        $result = $this->getRecords($domain, "AAAA");
        $temp = array();
        foreach($result as $ip){
                array_push($temp, array('AAAA' => $ip));
        }

        if($result){
                $res['AAAA'] = $temp;
        }
        
    }

    function getARecords($domain, &$res){
            $ares = array();

            $result = $this->getRecords($domain, "A");
            foreach($result as $ip){
                    $ptr = $this->getRecords($ip, "PTR");
                    if(count($ptr) > 0){
                            array_push($ares, array('A' => $ip, 'PTR' => $ptr[0]));
                    }else{
                            array_push($ares, array('A' => $ip, 'PTR' => 'None'));
                    }
            }
            if($result){
                    $res['A'] = $ares;
            }
    }

    function getNSRecords($domain, &$res){
            $result = $this->getRecords($domain, "NS");

            $temp = array();
            for($i = 0; $i<count($result); $i++){
                    $ns = $result[$i];
                    $a = $this->getRecords($ns, "A");
                    foreach($a as $ip){
                            array_push($temp, array('A' => $ip, 'NS' => $ns));
                    }
            }

            if($result){
                    $res['NS'] = $temp;
            }
    }

    function getMXRecords($domain, &$res){
            $result = $this->getRecords($domain, "MX");

            for($i = 0; $i<count($result); $i++){
                    $mx = $result[$i]['Record'];
                    $a = $this->getRecords($mx, "A");
                    foreach($a as $ip){
                            $result[$i]['A'] = $ip;
                            $ptr = $this->getRecords($ip, "PTR");
                            if(count($ptr)>0){
                                    $result[$i]['PTR'] = $ptr[0];
                            }else{
                                    $result[$i]['PTR'] = 'None';
                            }
                    }
            }
            if($result){
                    $res['MX'] = $result;
            }
    }

    function getTXTRecords($domain, &$res){
            $result = $this->getRecords($domain, "TXT");
            if($result){
                    $res['TXT'] = $result;
            }
    }

    function getSOARecord($domain, &$res){
            $result = $this->getRecords($domain, "SOA");
            if($result){
                    $res['SOA'] = $result;
            }
    }

    function getCAARecord($domain, &$res){
            $r = new \Net_DNS2_Resolver(array('nameservers' => array('8.8.8.8')));
            $result = $r->query($domain, 'CAA');

			if(count($result->answer) > 0){
				$res['CAA'] = array();
				foreach($result->answer as $record){
					array_push($res['CAA'], array('CAA' => sprintf("%d %s %s", $record->flags, $record->tag, $record->value)));
				}
			}	
    }

    public function getAllRecords(){
        $domain = $this->domain;
        
        if(Cache::has($domain)){
            return Cache::get($domain);
        }else{
            $this->getARecords($domain, $this->result_array);
            $this->getCAARecord($domain, $this->result_array);
            $this->getIPv6($domain, $this->result_array);
            $this->getNSRecords($domain, $this->result_array);
            $this->getMXRecords($domain, $this->result_array);
            $this->getTXTRecords($domain, $this->result_array);
			$this->getSOARecord($domain, $this->result_array);
			
            $empty = true;
            for($i = 0; $i < count($this->result_array); $i++){
                    if(!empty($this->result_array)){
                            $empty = false;
                    }
            }

            if($empty){
                    $this->result_array['error'] = "No DNS records were found for $domain";
                    Cache::put($domain, $this->result_array, 2);
                    return $this->result_array;
            }else{
                    Cache::put($domain, $this->result_array, 5);
                    return $this->result_array;
            }
        }
        
        
    }
}

?>