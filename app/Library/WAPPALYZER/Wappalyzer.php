<?php

namespace App\Library\WAPPALYZER;

use Cache;

class WAPPALYZER
{
    private $domain;

    public function __construct($domain){
        $this->domain = $domain;
    }

    public function lookup(){
        $cacheKey = sprintf("wappalyzer_%s", $this->domain);
        if(Cache::has($cacheKey)){
            return Cache::get($cacheKey);
        }else{
            $output = exec("cd ../app/Library/WAPPALYZER && node index.js http://" . $this->domain . " --quiet");
            $output = json_decode($output);
            Cache::put($cacheKey, $output, 600);
            return $output;
        }
    }
}