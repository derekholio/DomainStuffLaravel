<?php

namespace App\Library\SCREENSHOT;

class Screenshot
{
    private $domain;
    public function __construct($domain){
        $this->domain = $domain;
    }

    public function takeScreenshot(){
        $saveTo = 'screenshots/'.str_replace('.', '', $this->domain).'.jpg';
        if(file_exists($saveTo) && (time()-filemtime($saveTo) < 600)){
            return array('screenshot' => $saveTo);
        }
        
        $bin = env('PHANTOMJS_PATH', '');

        if(empty($bin)){
            new \Exception("Please specify the PHANTOMJS_PATH in .env");
        }

        $screenshot = new \Spatie\Browsershot\Browsershot();
        $screenshot
            ->setURL('http://'.$this->domain)
            ->setWidth('1920')
            ->setHeight('1080')
            ->setQuality(100)
            ->setBinPath($bin)
            ->save($saveTo);

        return array('screenshot' => $saveTo);
    }
}

?>