<?php

namespace App\Library\SSL;

class SSLChecker
{
    private $endpoint = "https://ssldecoder.org/json.php";
    private $domain;
    public function __construct($domain){
        $this->domain = $domain;
    }

    public function lookup(){
        $results = array();
        $url = sprintf("%s?host=%s&port=443&fastcheck=1", $this->endpoint, $this->domain);
        $contents = json_decode(file_get_contents($url));

        if(isset($contents->error)){
            $results['error'] = $contents->error;
            return $results;
        }

        $valid = $contents->data->connection->validation->status;

        $results['name'] = $contents->data->connection->chain[0]->name;
        $results['valid'] = ($valid === "success" ? true : false);
        $results['ip'] = $contents->data->connection->ip;

        if($valid){
            foreach($contents->data->chain as $cert){
                if(isset($results['expiration'])){
                    continue;
                }
                $date = gmdate('m/d/Y', str_replace("Z", "", $cert->cert_data->validTo_time_t));
                $results['expiration'] = $date;

                $results['signatureTypeSN'] = $cert->cert_data->signatureTypeSN;
                $results['signatureTypeLN'] = $cert->cert_data->signatureTypeLN;
                $SAN = array_map('trim', explode(",", str_replace("DNS:", "", $cert->cert_data->extensions->subjectAltName)));
                $results['SAN'] = $SAN;
                $results['warnings'] = $cert->warning;
                $results['issuer'] = $cert->cert_data->issuer->CN;
            }
        }
        return $results;
    }
}

?>