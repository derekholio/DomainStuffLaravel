<?php

namespace App\Library\PORTSCANNER;

class Scanner
{
    private $domain;
    private $ports = array(
        '21' => 'FTP',
        '22' => 'SSH',
        '25' => 'SMTP',
        '53' => 'DNS',
        '80' => 'HTTP',
        '110' => 'POP3',
        '143' => 'IMAP',
        '443' => 'HTTPS',
        '465' => 'SMTP',
        '995' => 'POP3',
        '1433' => 'MSSQL',
        '2082' => 'cPanel-HTTP',
        '2083' => 'cPanel-HTTPS',
        '2087' => 'WHM',
        '2525' => 'SMTP-ALT',
        '3306' => 'MySQL',
        '3389' => 'RDP',
        '8080' => 'HTTP-ALT',
        '8443' => 'Plesk-HTTPS',
        '8880' => 'Plesk-HTTP',
        '9998' => 'SmarterMail'
    );

    public function __construct($domain){
        $this->domain = $domain;
    }

    public function lookup(){
        $results = array();
        foreach($this->ports as $port => $service){
            $conn = @fsockopen($this->domain, $port, $errno, $errstr, .5);
            $result = array(
                'port' => $port,
                'service' => $service,
                'open' => is_resource($conn)
            );

            if($result['open']){
                fclose($conn);
            }

            array_push($results, $result);
        }

        return $results;
    }
}

?>