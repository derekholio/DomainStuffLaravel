<?php

namespace App\Library\REDIRECTCHECK;

class REDIRECTCHECK
{
    private $domain;
    private $maxAttempts = 10;
    private $locationPattern = '/^Location: (.*)/';

    public function __construct($domain){
        $this->domain = 'http://' . $domain;
    }

    public function lookup(){
        $results = array();

        try{
            for($i = 0; $i <= $this->maxAttempts; $i++){
                $client = new \GuzzleHttp\Client();
                $res = $client->get($this->domain, ['allow_redirects' => false]);
                $results[$i]['status'] = $res->getStatusCode();
                $results[$i]['phrase'] = $res->getReasonPhrase();
                $results[$i]['headers'] = $res->getHeaders();
                if($res->hasHeader('Location')){
                    $results[$i]['movedto'] = $res->getHeader('Location');
                    $this->domain = $res->getHeader('Location')[0];
                }else{
                    $results[$i]['movedto'] = $this->domain;
                    $i = 99;//stop
                }
            }
        }catch(\Exception $ex){
            $results['error'] = "Failed to conenct to " . $this->domain;
        }

        

        return $results;
    }
}

?>