<?php

namespace App\Library\DNSPROPAGATION;
use Cache;
use \App\Library\DNS;

class DNSPropagationChecker{
    private $domain;

    private $dnsServers = array(
        array('name' => 'Google (US)', 'server' => '8.8.8.8'),
        array('name' => 'Level 3 (US)', 'server' => '4.2.2.1'),
        array('name' => 'OpenDNS (US)', 'server' => '208.67.222.222'),
        array('name' => 'DNS.WATCH (GER)', 'server' => '84.200.69.80'),
        array('name' => 'iiNet (AU)', 'server' => '202.59.96.140'),
        array('name' => 'Seflow (IT)', 'server' => '95.141.39.238'),
        array('name' => 'Getflix (NZ)', 'server' => '120.138.27.84'),
        array('name' => 'Yandex (RU)', 'server' => '77.88.8.8')
    );

    public function __construct($domain){
        $this->domain = $domain;
    }

    public function lookup(){
        $results = array();
        foreach($this->dnsServers as $server){
            $temp_results = array();
            $dnsServer = $server['server'];
            $lookup = new DNS\DNSResolver($this->domain, $dnsServer);

            $temp = array();
            $lookup->getARecords($this->domain, $temp);
            if(array_key_exists("A", $temp)){
                $aRecords = $temp['A'];
                foreach($aRecords as $a){
                    array_push($temp_results, $a['A']);
                }
            }

            array_push($results, array('provider' => $server['name'], 'results' => $temp_results));
        }

        return $results;
    }
}


?>