<?php

namespace App\Library\BLACKLIST;

use Cache;

class BlacklistChecker
{
    private $ip;
    private $reversed_ip;

    private $blacklists = array(
        'b.barracudacentral.org',
        'bl.spamcop.net',
        'dnsbl-1.uceprotect.net',
        'dnsbl-2.uceprotect.net',
        'dnsbl-3.uceprotect.net',
        'dnsbl.sorbs.net',
        'ubl.unsubscore.com',
        'zen.spamhaus.org'
    );

    public function __construct($ip){
        $this->ip = $ip;
        $this->reversed_ip = implode(".",array_reverse(explode(".",$ip)));
    }

    public function lookup(){
        $key = sprintf("BL_%s", $this->reversed_ip);
        if(Cache::has($key)){
            return Cache::get($key);
        }else{
            $results = array();

            foreach($this->blacklists as $blacklist){
                $temp = array();
                $url = sprintf("%s.%s", $this->reversed_ip, $blacklist);
                $resolver = new \App\Library\DNS\DNSResolver($url);
                $a_records = $resolver->getARecords($url, $temp);
                if(empty($temp)){
                    $results[$blacklist] = array();
                }else{
                    if(array_key_exists("A", $temp)){
                        $results[$blacklist] = array();
                        $count = 0;
                        foreach($temp["A"] as $entry){
                            array_push($results[$blacklist], $temp["A"][$count]["A"]);
                        }
                    }else{
                        $results[$blacklist] = array();
                    }
                } 
            }

            Cache::put($key, $results, 60);
            return $results;
        }
        
    }
}

?>