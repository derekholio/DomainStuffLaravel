<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    Route::get('api/lookup/dns/{domain}', 'APIController@DNSLookup');
    Route::get('api/lookup/whois/{domain}', 'APIController@WHOISLookup');
    Route::get('api/lookup/geoip/{domain}', 'APIController@GeoIPLookup');
    Route::get('api/lookup/blacklist/{ip}', 'APIController@BlacklistLookup');
    Route::get('api/lookup/globallookup/{domain}', 'APIController@GlobalDNSLookup');
    Route::get('api/lookup/portscan/{domain}', 'APIController@PortScanLookup');
    Route::get('api/lookup/wappalyzer/{domain}', 'APIController@WappalyzerLookup');
    Route::get('api/lookup/redirect/{domain}', 'APIController@RedirectLookup');
    Route::get('api/lookup/ssl/{domain}', 'APIController@SSLLookup');
    Route::get('api/lookup/screenshot/{domain}', 'APIController@Screenshot');

    Route::get('admin', function(){
        return view('layouts.admin');
    });

    Route::get('{domain}', function($domain){
        $data = array(
            'domain' => $domain,
        );
        
        return view("domain_results", $data);
    });
});
