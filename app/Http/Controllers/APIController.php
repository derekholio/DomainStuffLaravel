<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class APIController extends Controller
{
    public function GeoIPLookup($domain){
        $geoip = new \App\Library\GEOIP\IPLocator($domain);
        return response()->json($geoip->lookup());
    }

    public function WHOISLookup($domain){
        try{
            $whois = new \App\Library\WHOIS\DSWHOIS($domain);
            $lookup = $whois->lookup();
            return '<pre>' . $lookup . '</pre>';
        } catch(InvalidArgumentException $iex){
            return "<pre>Invalid domain: $domain</pre>";
        } catch (Exception $ex) {
            Log::Error("Failed to get whois: $domain");
            Log::Error($ex->getFile());
            Log::Error($ex->getMessage());
            
            return "<pre>An error was returned while retrieving WHOIS for $domain.</pre>";
        }
    }

    public function DNSLookup($domain){
        $resolver = new \App\Library\DNS\DNSResolver($domain);
        return response()->json($resolver->getAllRecords());
    }

    public function BlacklistLookup($ip){
        $lookup = new \App\Library\BLACKLIST\BlacklistChecker($ip);
        return response()->json($lookup->lookup());
    }

    public function GlobalDNSLookup($domain){
        $lookup = new \App\Library\DNSPROPAGATION\DNSPropagationChecker($domain);
        return response()->json($lookup->lookup());
    }

    public function PortScanLookup($domain){
        $lookup = new \App\Library\PORTSCANNER\Scanner($domain);
        return response()->json($lookup->lookup());
    }

    public function WappalyzerLookup($domain){
        $lookup = new \App\Library\WAPPALYZER\Wappalyzer($domain);
        return response()->json($lookup->lookup());
    }

    public function RedirectLookup($domain){
        $lookup = new \App\Library\REDIRECTCHECK\RedirectCheck($domain);
        return response()->json($lookup->lookup());
    }

    public function SSLLookup($domain){
        $lookup = new \App\Library\SSL\SSLChecker($domain);
        return response()->json($lookup->lookup());
    }

    public function Screenshot($domain){
        $screenshot = new \App\Library\SCREENSHOT\Screenshot($domain);
        return response()->json($screenshot->takeScreenshot());
    }
}
